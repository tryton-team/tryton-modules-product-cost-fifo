tryton-modules-product-cost-fifo (7.0.1-3) unstable; urgency=medium

  * Use unittest.discover to run autopkgtests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 25 Oct 2024 12:55:48 +0200

tryton-modules-product-cost-fifo (7.0.1-2) experimental; urgency=medium

  * Upload to experimental.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 21 Oct 2024 07:55:24 +0200

tryton-modules-product-cost-fifo (7.0.1-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.7.0, no changes needed.
  * Setting the branch in the watch file to the fixed version 7.0.
  * Remove deprecated python3-pkg-resources from (Build)Depends (and wrap-
    and-sort -a) (Closes: #1083886).
  * Merging upstream version 7.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Oct 2024 13:46:41 +0200

tryton-modules-product-cost-fifo (6.0.3-1) unstable; urgency=medium

  * Switch to pgpmode=auto in the watch file.
  * Update year of debian copyright.
  * Switch to pgpmode=none in the watch file.
  * Merging upstream version 6.0.3.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 24 Jul 2024 10:27:29 +0200

tryton-modules-product-cost-fifo (6.0.2-2) sid; urgency=medium

  * Add a salsa-ci.yml
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Repository.
  * Update standards version to 4.6.1, no changes needed.
  * Unify the Tryton module layout.
  * Update the package URLS to https and the new mono repos location.
  * Bump the Standards-Version to 4.6.2, no changes needed.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 13 Feb 2023 13:08:04 +0100

tryton-modules-product-cost-fifo (6.0.2-1) unstable; urgency=medium

  * Merging upstream version 6.0.2.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 09 Sep 2022 10:50:38 +0200

tryton-modules-product-cost-fifo (6.0.1-2) unstable; urgency=medium

  * Use debhelper-compat (=13).
  * Depend on the tryton-server-api of the same major version.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 12 Nov 2021 21:53:04 +0100

tryton-modules-product-cost-fifo (6.0.1-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.6.0, no changes needed.
  * Set the watch file version to 4.
  * Setting the branch in the watch file to the fixed version 6.0.
  * Merging upstream version 6.0.1.
  * Use same debhelper compat as for server and client.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 19 Oct 2021 12:12:53 +0200

tryton-modules-product-cost-fifo (5.0.10-1) unstable; urgency=medium

  * Updating to standards version 4.5.1, no changes needed.
  * Merging upstream version 5.0.10.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 29 Jun 2021 10:55:15 +0200

tryton-modules-product-cost-fifo (5.0.9-1) unstable; urgency=medium

  * Merging upstream version 5.0.9.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Dec 2020 09:56:08 +0100

tryton-modules-product-cost-fifo (5.0.8-1) unstable; urgency=medium

  * Merging upstream version 5.0.8.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 26 Sep 2020 10:06:10 +0200

tryton-modules-product-cost-fifo (5.0.7-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.5.0, no changes needed.
  * Add Rules-Requires-Root: no to d/control.
  * Merging upstream version 5.0.7.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 04 Jul 2020 12:56:21 +0200

tryton-modules-product-cost-fifo (5.0.6-1) unstable; urgency=medium

  * Merging upstream version 5.0.6.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 05 Apr 2020 12:23:49 +0200

tryton-modules-product-cost-fifo (5.0.5-1) unstable; urgency=medium

  * Merging upstream version 5.0.5.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 03 Dec 2019 10:55:59 +0100

tryton-modules-product-cost-fifo (5.0.4-2) unstable; urgency=medium

  * Remove 01-add-package-tests.patch, patch went upstream.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 25 Jul 2019 09:47:54 +0200

tryton-modules-product-cost-fifo (5.0.4-1) unstable; urgency=medium

  * Merging upstream version 5.0.4.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 23 Jul 2019 18:26:10 +0200

tryton-modules-product-cost-fifo (5.0.2-2) unstable; urgency=medium

  * Bump the Standards-Version to 4.4.0, no changes needed.
  * Update year of debian copyright.
  * Setting the branch in the watch file to the fixed version 5.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 22 Jul 2019 12:17:10 +0200

tryton-modules-product-cost-fifo (5.0.2-1) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Merging upstream version 5.0.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 03 Apr 2019 14:08:51 +0200

tryton-modules-product-cost-fifo (5.0.1-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.3.0, no changes needed.
  * Merging upstream version 5.0.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 20 Feb 2019 10:24:24 +0100

tryton-modules-product-cost-fifo (5.0.0-3) unstable; urgency=medium

  * Add 01-add-package-tests.patch.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 04 Jan 2019 21:00:04 +0100

tryton-modules-product-cost-fifo (5.0.0-2) unstable; urgency=medium

  * Cleanup white space.
  * Add a generic autopkgtest to run the module testsuite.
  * Update the rules file with hints about and where to run tests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 01 Jan 2019 20:35:18 +0100

tryton-modules-product-cost-fifo (5.0.0-1) unstable; urgency=medium

  * Merging upstream version 5.0.0.
  * Updating copyright file.
  * Updating to Standards-Version: 4.2.1, no changes needed.
  * Update signing-key.asc with the minimized actual upstream maintainer
    key.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 20 Nov 2018 17:08:12 +0100

tryton-modules-product-cost-fifo (4.6.0-2) unstable; urgency=medium

  * Update year of debian copyright.
  * Updating to standards version 4.1.3, no changes needed.
  * control: update Vcs-Browser and Vcs-Git
  * Set the Maintainer address to team+tryton-team@tracker.debian.org.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 29 Mar 2018 21:14:54 +0200

tryton-modules-product-cost-fifo (4.6.0-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.1.0, no changes needed.
  * Bump the Standards-Version to 4.1.1, no changes needed.
  * Merging upstream version 4.6.0.
  * Use https in the watch file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 07 Nov 2017 10:20:16 +0100

tryton-modules-product-cost-fifo (4.4.0-4) unstable; urgency=medium

  * Switch to Python3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 18 Aug 2017 12:35:29 +0200

tryton-modules-product-cost-fifo (4.4.0-3) unstable; urgency=medium

  * Use the preferred https URL format in the copyright file.
  * Bump the Standards-Version to 4.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Aug 2017 16:02:51 +0200

tryton-modules-product-cost-fifo (4.4.0-2) unstable; urgency=medium

  * Change the maintainer address to tryton-debian@lists.alioth.debian.org
    (Closes: #865109).

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Jul 2017 12:36:13 +0200

tryton-modules-product-cost-fifo (4.4.0-1) unstable; urgency=medium

  * Merging upstream version 4.4.0.
  * Updating debian/copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 10 Jun 2017 23:29:50 +0200

tryton-modules-product-cost-fifo (4.2.1-1) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Merging upstream version 4.2.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 14 Mar 2017 10:43:52 +0100

tryton-modules-product-cost-fifo (4.2.0-1) unstable; urgency=medium

  * Updating to Standards-Version: 3.9.8, no changes needed.
  * Merging upstream version 4.2.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 05 Dec 2016 15:31:24 +0100

tryton-modules-product-cost-fifo (4.0.1-1) unstable; urgency=medium

  * Updating signing-key.asc with the actual upstream maintainer keys.
  * Merging upstream version 4.0.0.
  * Merging upstream version 4.0.1.
  * Updating the copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 30 May 2016 19:29:56 +0200

tryton-modules-product-cost-fifo (3.8.0-2) unstable; urgency=medium

  * Updating to standards version 3.9.7, no changes needed.
  * Updating VCS-Browser to https and canonical cgit URL.
  * Updating VCS-Git to https and canonical cgit URL.
  * Removing the braces from the dh call.
  * Removing the version constraint from python.
  * Disable tests globally for all Python versions.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 08 Mar 2016 13:09:01 +0100

tryton-modules-product-cost-fifo (3.8.0-1) unstable; urgency=medium

  * Updating year of debian copyright.
  * Adapting section naming in gbp.conf to current git-buildpackage.
  * Improving description why we can not run the module test suites.
  * Merging upstream version 3.8.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 12 Nov 2015 19:13:35 +0100

tryton-modules-product-cost-fifo (3.6.0-1) unstable; urgency=medium

  * Wrapping and sorting control files (wrap-and-sort -bts).
  * Merging upstream version 3.6.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 26 Apr 2015 23:49:24 +0200

tryton-modules-product-cost-fifo (3.4.1-1) unstable; urgency=medium

  * Adding actual upstream signing key.
  * Improving boilerplate in d/control (Closes: #771722).
  * Merging upstream version 3.4.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 24 Feb 2015 20:08:24 +0100

tryton-modules-product-cost-fifo (3.4.0-1) unstable; urgency=medium

  * Merging upstream version 3.4.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 21 Oct 2014 20:24:13 +0200

tryton-modules-product-cost-fifo (3.2.2-1) unstable; urgency=medium

  * Updating signing key while using now plain .asc files instead of .pgp
    binaries.
  * Adding actual upstream signing key.
  * Updating to Standards-Version: 3.9.6, no changes needed.
  * Merging upstream version 3.2.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 08 Oct 2014 13:38:35 +0200

tryton-modules-product-cost-fifo (3.2.1-1) unstable; urgency=medium

  * Bumping package version to correct upstream version.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 28 Apr 2014 18:04:59 +0200

tryton-modules-product-cost-fifo (3.2.0-1) unstable; urgency=medium

  * Removing  LC_ALL=C.UTF-8 as build environment.
  * Merging upstream version 3.2.1.
  * Updating copyright.
  * Bumping minimal required Python version to 2.7.
  * Updating gbp.conf for usage of upstream tarball compression.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 24 Apr 2014 15:28:03 +0200

tryton-modules-product-cost-fifo (3.0.0-3) unstable; urgency=medium

  * Updating year in debian copyright.
  * Removing debian/source/options, we are building with dpkg defaults.
  * Removing PYBUILD_DESTDIR_python2 from rules, it is no more needed.
  * Adding pgp verification for uscan.
  * Adding gbp.conf for usage with git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Mar 2014 17:15:44 +0100

tryton-modules-product-cost-fifo (3.0.0-2) unstable; urgency=low

  * Pointing VCS fields to new location on alioth.debian.org.
  * Using dpkg defaults for xz compression.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 02 Dec 2013 21:16:40 +0100

tryton-modules-product-cost-fifo (3.0.0-1) unstable; urgency=low

  * Merging upstream version 3.0.0.
  * Updating to standards version 3.9.5, no changes needed.
  * Changing to buildsystem pybuild.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 25 Nov 2013 17:54:31 +0100

tryton-modules-product-cost-fifo (2.8.1-1) unstable; urgency=low

  * Adapting the rules file to work also with git-buildpackage.
  * Merging upstream version 2.8.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 05 Aug 2013 21:25:47 +0200

tryton-modules-product-cost-fifo (2.8.0-2) unstable; urgency=low

  * Adding doc/ to docs file.
  * Simplifying package layout by renaming <pkg_name>.docs to docs.
  * Removing needless empty line in rules.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 31 May 2013 17:26:49 +0200

tryton-modules-product-cost-fifo (2.8.0-1) experimental; urgency=low

  * Merging upstream version 2.8.0.
  * Updating copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 02 May 2013 15:20:25 +0200

tryton-modules-product-cost-fifo (2.6.0-4) experimental; urgency=low

  * Removing Daniel from Uploaders. Thanks for your work! (Closes: #704389).
  * Improving update of major version in Depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 27 Apr 2013 15:07:44 +0200

tryton-modules-product-cost-fifo (2.6.0-3) experimental; urgency=low

  * Updating Vcs-Git to correct address.
  * Adding watch file. Thanks to Bart Martens <bartm@debian.org>.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 23 Mar 2013 14:00:55 +0100

tryton-modules-product-cost-fifo (2.6.0-2) experimental; urgency=low

  * Removing obsolete Dm-Upload-Allowed
  * Updating to Standards-Version: 3.9.4, no changes needed.
  * Updating copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 16 Feb 2013 21:41:10 +0100

tryton-modules-product-cost-fifo (2.6.0-1) experimental; urgency=low

  * Merging upstream version 2.6.0.
  * Bumping versioned tryton depends to 2.6.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 24 Oct 2012 14:24:46 +0200

tryton-modules-product-cost-fifo (2.4.0-2) experimental; urgency=low

  [ Daniel Baumann ]
  * Updating maintainers field.
  * Updating vcs fields.
  * Correcting copyright file to match format version 1.0.
  * Switching to xz compression.
  * Updating to debhelper version 9.

  [ Mathias Behrle ]
  * Merging branch debian-wheezy-2.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 18 Sep 2012 13:39:42 +0200

tryton-modules-product-cost-fifo (2.4.0-1) experimental; urgency=low

  * Updating to Standards-Version: 3.9.3, no changes needed.
  * Updating year in copyright.
  * Adding myself to copyright.
  * Adding Format header for DEP5.
  * Merging upstream version 2.4.0.
  * Updating Copyright.
  * Bumping versioned tryton depends to 2.4.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 26 Apr 2012 19:31:16 +0200

tryton-modules-product-cost-fifo (2.2.0-1) unstable; urgency=low

  * Bumping X-Python-Version to >=2.6.
  * Updating versioned tryton depends to 2.2.
  * Merging upstream version 2.2.0.
  * Removing deprecated XB-Python-Version for dh_python2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 26 Dec 2011 14:01:36 +0100

tryton-modules-product-cost-fifo (2.0.1-2) unstable; urgency=low

  [ Daniel Baumann ]
  * Removing for new source package version obsoleted README.source file.
  * Adding options for source package.
  * Compacting copyright file.
  * Not wrapping uploaders field, it does not exceed 80 chars.

  [ Mathias Behrle ]
  * Moving from deprecated python-support to dh_python2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 14 Jul 2011 01:20:14 +0200

tryton-modules-product-cost-fifo (2.0.1-1) unstable; urgency=low

  * Merging upstream version 2.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 05 Jun 2011 13:18:27 +0200

tryton-modules-product-cost-fifo (2.0.0-1) unstable; urgency=low

  * Updating to standards version 3.9.2.
  * Merging upstream version 2.0.0.
  * Updating versioned tryton depends to 2.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 24 May 2011 21:35:14 +0200

tryton-modules-product-cost-fifo (1.8.1-1) unstable; urgency=low

  * Changing my email address.
  * Setting minimal Python version to 2.5.
  * Merging upstream version 1.8.1.
  * Updating Copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 15 Feb 2011 13:12:06 +0100

tryton-modules-product-cost-fifo (1.8.0-1) experimental; urgency=low

  * Updating standards version to 3.9.0.
  * Updating to debhelper version 8.
  * Updating to standards version 3.9.1.
  * Switching to source format 3.0 (quilt).
  * Merging upstream version 1.8.0.
  * Updating versioned tryton depends to 1.8.

 -- Daniel Baumann <daniel@debian.org>  Fri, 12 Nov 2010 13:05:56 +0100

tryton-modules-product-cost-fifo (1.6.0-1) unstable; urgency=low

  [ Daniel Baumann ]
  * Adding Dm-Upload-Allowed in control in preparation for Mathias.

  [ Mathias Behrle ]
  * Merging upstream version 1.6.0.
  * Updating copyright.
  * Updating depends.

 -- Mathias Behrle <mathiasb@mbsolutions.selfip.biz>  Thu, 13 May 2010 12:18:49 +0200

tryton-modules-product-cost-fifo (1.4.0-2) unstable; urgency=low

  * Making depends versioned for tryton 1.4.
  * Adding explicit debian source version 1.0 until switch to 3.0.
  * Updating year in copyright file.
  * Removing unneeded python-all-dev from build-depends.
  * Updating to standards 3.8.4.
  * Updating README.source.

 -- Daniel Baumann <daniel@debian.org>  Sat, 20 Feb 2010 10:42:11 +0100

tryton-modules-product-cost-fifo (1.4.0-1) unstable; urgency=low

  * Merging upstream version 1.4.0.

 -- Daniel Baumann <daniel@debian.org>  Mon, 19 Oct 2009 22:19:16 +0200

tryton-modules-product-cost-fifo (1.2.0-3) unstable; urgency=low

  * Updating to standards version 3.8.3.
  * Adding maintainer homepage field to control.
  * Adding README.source.
  * Moving maintainer homepage field to copyright.
  * Updating README.source.

 -- Daniel Baumann <daniel@debian.org>  Sat, 05 Sep 2009 09:42:10 +0200

tryton-modules-product-cost-fifo (1.2.0-2) unstable; urgency=low

  * Updating maintainer field.
  * Updating vcs fields.
  * Wrapping lines in control.

 -- Daniel Baumann <daniel@debian.org>  Mon, 10 Aug 2009 20:18:53 +0200

tryton-modules-product-cost-fifo (1.2.0-1) unstable; urgency=low

  * Initial release.

 -- Daniel Baumann <daniel@debian.org>  Sun, 02 Aug 2009 01:06:00 +0200
